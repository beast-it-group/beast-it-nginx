ARG BASE_IMAGE="nginx:1.21-alpine"
FROM ${BASE_IMAGE}

WORKDIR /var/www

RUN adduser -S -G www-data www-data \
    && chown -R www-data /var/cache/nginx/ \
    && chown -R www-data:www-data /var/run/ \
    && rm /etc/nginx/conf.d/default.conf

USER www-data

COPY server-configs-nginx/ /etc/nginx/
COPY sites/default.conf /etc/nginx/conf.d/default.conf

CMD ["nginx", "-g", "daemon off;"]
